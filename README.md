This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

I've included the bundles in the email to Nickolas, but to make life somewhat easier, I've loaded both apps to AWS (free tier). 

React app: http://18.191.213.60:5000/
Express back end: http://18.219.47.69:8080 (GET '/' is a valid endpoint)

The back end is connected to a MySQL instance that also sits on AWS.

Additional npm packages used:
- bootstrap
- react-outer
- react-images-upload (a small component for the uploader)
- cors (on the express back end to allow for CORS)
- body-parser (on express, to parse POST bodies)
- express-fileupload (to enable file uploading)