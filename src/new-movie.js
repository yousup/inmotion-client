import React, { Component } from 'react';
import { MovieForm, HttpStatus } from './util/html';
import { createMovie, uploadImage } from './util/http';
import { checkRatingFormat, checkYearFormat } from './util/util';

export class NewMovie extends Component {
	constructor(props) {
		super(props);
		this.state = {}
		this.handleChange = this.handleChange.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
		this.onDrop = this.onDrop.bind(this);
	}

	handleChange = (e) => {
		const newState = {};
		newState[e.target.name] = e.target.value;
		
		if (e.target.name === "year") {
			newState.invalidYearFormat = checkYearFormat(e.target.value);
		}
		if (e.target.name === "rating") {
			newState.invalidRatingFormat = checkRatingFormat(e.target.value);
		}
		this.setState(newState);
	}

	handleSubmit = async (e) => {
		this.setState({
			status: "sending"
		})
		e.preventDefault(); // Prevents page refresh
		const resp = await createMovie(this.state);
		this.setState(resp);
		if (this.state.image) {
			uploadImage(this.state.image, resp.id)
		}
	}

	onDrop = (image) => {
		this.setState({
			image: image[0]
		})
	}

	// Front end required fields validation. Backend for extra security is recommended
	requiredFields = () => {
		return this.state.title && this.state.year && this.state.genre && this.state.invalidYearFormat !== true && this.state.invalidRatingFormat != true;
	}

	render() {
		return (
			<div className="container newmovie-form-container">
			<a href="/"><button className="btn btn-primary back-button">Back</button></a>
			<MovieForm 
				handleSubmit={this.handleSubmit}
				handleChange={this.handleChange}
				onDrop={this.onDrop}
				invalidYearFormat={this.state.invalidYearFormat}
				invalidRatingFormat={this.state.invalidRatingFormat}
				requiredFields={this.requiredFields}>
			</MovieForm>
			<HttpStatus
				status={this.state.status}
				statusMessage={this.state.statusMessage}>
			</HttpStatus>
		</div>
		)
	}
}