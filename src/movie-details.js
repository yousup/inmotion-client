import React, { Component } from "react";
import { getMovieImage } from './util/http';
import ImageLoader from 'react-image-file';

export class MovieDetails extends Component {
	constructor(props) {
		super(props)
		this.state = {}
	}

	componentDidMount = async () => {
		// retrieve the image. It's not the most efficient, but it works
		const image = await getMovieImage(this.props.location.state.movie.id);
		let base64url;
		if (image.image[0].image) {
			let buffer = Buffer.from( image.image[0].image);
			let bufferBase64 = buffer.toString('base64');
			base64url = "data:image/jpeg;base64,"+bufferBase64;
		}
		else {
			base64url = null;
		}
	
		this.setState({
			id: this.props.location.state.movie.id,
			title: this.props.location.state.movie.title,
			genre: this.props.location.state.movie.genre,
			rating: this.props.location.state.movie.rating,
			actors: this.props.location.state.movie.actors,
			year: this.props.location.state.movie.year,
			image: image ? base64url : null
		})
	}

	render () {
		return(
			<div className="container movie-details-container">
				<a href="/"><button className="btn btn-primary back-button">Back</button></a>
				<Details
					title={this.state.title}
					image={this.state.image}
					year={this.state.year}
					genre={this.state.genre}
					rating={this.state.rating}
					actors={this.state.actors}></Details>
			</div>
		)
	}
}

const Details = (props) => {
	return <div>
		{props.image && <img src={props.image} width="60%" ></img>}
		<h1>{props.title}</h1>
		<h5>Year released: {props.year}</h5>
		<h5>Genre: {props.genre}</h5>
		<h5>Rating: {props.rating}/10</h5>
		<h5>Featuring: {props.actors}</h5>
	</div>
}