import React, { Component } from 'react';
import { getMovies, deleteMovie } from './util/http';
import { SearchForm } from './util/html';
import { Button, Modal } from 'react-bootstrap';
import { Link } from "react-router-dom";

export class AllMovies extends Component {
	constructor(props) {
		super(props)
		this.state = {}
		this.handleShow = this.handleShow.bind(this);
		this.handleClose = this.handleClose.bind(this);
		this.handleDelete = this.handleDelete.bind(this);
		this.handleSearchChange = this.handleSearchChange.bind(this)
	}

	componentDidMount = async () => {
		this.setState(await getMovies());
	}

	handleClose() {
    this.setState({
			idToDelete: null,
			show: false
		});
  }

  handleShow = (param) => (e) => {
    this.setState({
			idToDelete: param,
			show: true
		});
	}

	handleSearchChange = (e) => {
		const search = e.target.value.toLowerCase();
		let actors;
		let rating;
		
		const filteredMovies = this.state.allMovies.filter(movie => {
			// actors and rating are nullable fields
			if (!movie.actors) actors = "";
			else {
				actors = movie.actors;
			}
			if (!movie.rating) rating = "";
			else {
				rating = movie.rating;
			}

			if (search.length > 0) {
				return movie.title.toLowerCase().includes(search) === true || movie.genre.toLowerCase().includes(search) === true || actors.toLowerCase().includes(search) === true || movie.year.includes(search) === true || rating.includes(search) === true
			}
			else {
				// don't search unless query is greater than length 0
				return true;
			}
		})
		this.setState({
			movies: filteredMovies
		})
	}

	handleDelete = async () => {
		const resp = await deleteMovie(this.state.idToDelete);
		if (resp) {
			this.setState(resp);
		}
	}

	render() {
		return (
			<div className="container allmovies-form-container">
				{this.state.movies && <a href="/new"><button className="btn btn-primary add-movie-button">Add Movie</button></a>}
				<SearchForm
					handleSearchChange={this.handleSearchChange}>
				</SearchForm>
				{this.state.movies && <MovieList movies={this.state.movies} handleShow={this.handleShow.bind(this)}></MovieList>}
				<DeleteModal
					id={this.state.idToDelete} 
					show={this.state.show}
					handleDelete={this.handleDelete.bind(this)}
					handleClose={this.handleClose.bind(this)}>
				</DeleteModal>
			</div>
		)
	}
}

const MovieList = (props) => {
	return <div className="list-group">
		{props.movies.map(movie => <MovieSingleRow key={movie.id} movie={movie} handleShow={props.handleShow}></MovieSingleRow>)}
	</div>
}

const MovieSingleRow = (props) => {
	return <div key={props.movie.title}>
		<div className="list-group-item list-group-item-action movies-list">
			<span className="movies-listitem-title">{props.movie.title}&nbsp;</span>
			<span className="movies-listitem-title">({props.movie.year})</span><br/>
			<Link to={{pathname: "/details/"+props.movie.id, state:{movie: props.movie}}}>View</Link>&nbsp;&nbsp;
			<Link to={{pathname: "/update/" + props.movie.id, state:{movie: props.movie}}}>Edit</Link>&nbsp;&nbsp;
			<span><a href="#" className="movies-listitem-deletelink" onClick={props.handleShow(props.movie.id)}>Delete</a></span>
		</div>
	</div>
}

const DeleteModal = (props) => {
	{/* The modal for delete confirmation */}
	return <Modal show={props.show} onHide={props.handleClose}>
		<Modal.Body>Are you sure you want to delete this movie?</Modal.Body>
		<Modal.Footer>
			<Button variant="secondary" onClick={props.handleClose}>
				Close
			</Button>
			<Button variant="danger" onClick={props.handleDelete}>
				Delete
			</Button>
		</Modal.Footer>
	</Modal>
}