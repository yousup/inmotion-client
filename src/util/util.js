export const checkYearFormat = (year) => {
	let regex = /^[\d]{4}$/g;
	if (year && year.length > 0 && year.match(regex) === null) {
		return true;
	}
	else {
		return false;
	}
}

export const checkRatingFormat = (rating) => {
	if (rating) {
		if ((rating.match(/^[\d]{1}$/g) === null && rating !== "10") || rating === "0") {
			return true;
		}
		else {
			return false;
		}
	}
	else {
		return false;
	}
}