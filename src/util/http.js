const url = "18.219.47.69:8080";

export const getMovies = async () => {
	try {
		const res = await fetch("http://"+url+"/", {
			method: "GET",
			headers: {'Content-Type': 'application/json'}
		})
		if (!res.ok) {
			throw Error("network request failed")
		}
		else {
			let data = await res.json();
			if (data.error)
				console.log(data.error.err)
			else
				return {
					movies: data.movies,
					allMovies: data.movies
				}
		}
	}
	catch (err) {
		return {requestFailed: true};
	}
}

export const createMovie = async (state) => {
	try {
		const res = await fetch("http://"+url+"/create",{
			method: "POST",
			headers: {'Content-Type':'application/json'},
			body: JSON.stringify({
				title: state.title,
				year: state.year,
				actors: state.actors,
				genre: state.genre,
				rating: state.rating,
			})
		})
		if (!res.ok) {
			throw Error("Network Request Failed");
		}
		else {
			let data = await res.json();
			return {
				id: data.id,
				status: data.status,
				statusMessage: data.statusMessage,
			}
		}
	}
	catch (err) {
		return {requestFailed: true}
	}
}

export const uploadImage = async (image, id) => {
	console.log(id)
	var data = new FormData()
	data.append('file', image)

	try {
		const res = await fetch("http://"+url+"/imageUpload/"+id, {
			method: 'POST',
			body: data
		})
		if (!res.ok) {
			throw Error("Network Request Failed");
		}
		else {
			let data = await res.json();
			return {
				status: data.status,
				statusMessage: data.statusMessage,
			}
		}
	}
	catch (err) {
		return {requestFailed: true}
	}
}

export const deleteMovie = async (id) => {
	try {
		let res = await fetch("http://"+url+"/delete/"+id, {
			method: "DELETE",
			headers: {'Content-Type': 'application/json'}
		})
		if (!res.ok) {
			throw Error("network request failure")
		}
		else {
			let data = await res.json();
			if (data.error) {
				console.log(data.error.err);
				return {
					status: "failed",
					statusMessage: "There was an error deleting the movie"
				}
			}
			else {
				console.log(data);
				window.location.reload();
			}
		}
	}
	catch (err) {
		return {requestFailed: true};
	}
}

export const updateMovie = async (id, state) => {
	try {
		const res = await fetch("http://"+url+"/update/"+id,{
			method: "PATCH",
			headers: {'Content-Type':'application/json'},
			body: JSON.stringify({
				title: state.title,
				year: state.year,
				actors: state.actors,
				genre: state.genre,
				rating: state.rating
			})
		})
		if (!res.ok) {
			throw Error("Network Request Failed");
		}
		else {
			let data = await res.json();
			return {
				status: data.status,
				statusMessage: data.statusMessage,
			}
		}
	}
	catch (err) {
		return {requestFailed: true}
	}
}

export const getMovieImage = async (id) => {
	try {
		const res = await fetch("http://"+url+"/image/"+id, {
			method: "GET",
			headers: {'Content-Type': 'application/json'}
		})
		if (!res.ok) {
			throw Error("network request failed")
		}
		else {
			let data = await res.json();
			if (data.error)
				console.log(data.error.err)
			else
				return {
					image: data.image,
				}
		}
	}
	catch (err) {
		return {requestFailed: true};
	}
}