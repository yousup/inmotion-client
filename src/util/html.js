import React from 'react';
import LoadingWheel from '../images/loading-wheel.svg';
import ImageUploader from 'react-images-upload';

export const MovieForm = (props) => {
	return <form onSubmit={props.handleSubmit}>
		<div className="form-group">
			<label htmlFor="inputTitle">Title</label>
			<input type="text" className="form-control" id="inputTitle" onChange={props.handleChange} value={props.title} placeholder="Title" name="title"/>
		</div>
		<div className="form-group">
			<label htmlFor="inputYear">Year</label>
			<input type="text" className="form-control" id="inputYear" onChange={props.handleChange} name="year" value={props.year} placeholder="YYYY"/>
			{props.invalidYearFormat && <p className="validation-error">Date must be in YYYY format</p>}
		</div>
		<div className="form-group">
			<label htmlFor="inputGenre">Genre</label>
			<select className="custom-select" name="genre" id="inputGenre" onChange={props.handleChange} value={props.genre}>
				<option disabled selected>Select One</option>
				<option value="Comedy">Comedy</option>
				<option value="RomCom">Rom/Com</option>
				<option value="Horror">Horror</option>
				<option value="Drama">Drama</option>
				<option value="Documentary">Documentary</option>
				<option value="Animated">Animated</option>
				<option value="Action">Action</option>
				<option value="Adventure">Adventure</option>
				<option value="Other">Other</option>
			</select>
		</div>
		<div className="form-group">
			<label htmlFor="inputImage">Movie Poster</label>
			<ImageUploader
				name="image"
				id="inputImage"
				withIcon={true}
				singleImage={true}
				buttonText='Choose image'
				onChange={props.onDrop}
				imgExtension={['.jpg']}
				maxFileSize={5242880}
		/></div>
		<div className="form-group">
			<label htmlFor="inputRating">Rating</label>
			<input type="text" className="form-control" id="inputRating" onChange={props.handleChange} placeholder="Rating" value={props.rating} name="rating"/>
			{props.invalidRatingFormat && <p className="validation-error">Rating must be a number between 1-10</p>}
		</div>
		<div className="form-group">
			<label htmlFor="inputActors">Actors</label>
			<textarea className="form-control" placeholder="Actors" id="inputActors" onChange={props.handleChange} rows="4" value={props.actors} name="actors"></textarea>
		</div>
		{props.requiredFields() && <button className="btn btn-light movie-submit">Submit</button>}
		{!props.requiredFields() && <button className="btn btn-danger movie-submit" disabled>Required Fields Incomplete</button>}
	</form>
}

export const SearchForm = (props) => {
	return <form>
		<div className="form-group">
			<label htmlFor="inputSearch"></label>
			<input type="text" className="form-control" id="inputSearch" onChange={props.handleSearchChange} value={props.title} placeholder="Search (e.g 'Titanic', '1995', 'Daniel Radcliffe')" name="search"/>
		</div>
	</form>
}

export const HttpStatus = (props) => {
	return (
		<div>
			<div className="loading-wheel-div">
				{props.status === "sending" && <img src={LoadingWheel}/>}
			</div>
			<div className="http-success-message">
				{props.status === "success" && <span>{props.statusMessage}</span>}
			</div>
			<div className="http-failed-message">
				{props.status === "failed" && <span>{props.statusMessage}</span>}
			</div>
		</div>
	)
}