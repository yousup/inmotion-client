import React, { Component } from 'react';
import { MovieForm, HttpStatus } from './util/html';
import { updateMovie, uploadImage } from './util/http';
import { checkRatingFormat, checkYearFormat } from './util/util';

export class UpdateMovie extends Component {
	constructor(props) {
		super(props);
		this.state = {}
		this.handleChange = this.handleChange.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
	}

	handleChange = (e) => {
		const newState = {};
		newState[e.target.name] = e.target.value;
		if (e.target.name === "year") {
			newState.invalidYearFormat = checkYearFormat(e.target.value);
		}
		if (e.target.name === "rating") {
			newState.invalidRatingFormat = checkRatingFormat(e.target.value);
		}
		this.setState(newState);
	}

	handleSubmit = async (e) => {
		this.setState({
			status: "sending"
		})
		e.preventDefault(); // Prevents page refresh
		const resp = await updateMovie(this.state.id, this.state);
		this.setState(resp);
		if (this.state.image) {
			console.log("here")
			uploadImage(this.state.image, this.state.id)
		}
	}

	// Front end required fields validation. Backend for extra security is recommended
	requiredFields = () => {
		return this.state.title && this.state.year && this.state.genre && this.state.invalidYearFormat !== true && this.state.invalidRatingFormat != true;
	}

	onDrop = (image) => {
		this.setState({
			image: image[0]
		})
	}

	componentDidMount = () => {
		this.setState({
			id: this.props.location.state.movie.id,
			title: this.props.location.state.movie.title,
			genre: this.props.location.state.movie.genre,
			rating: this.props.location.state.movie.rating,
			actors: this.props.location.state.movie.actors,
			year: this.props.location.state.movie.year
		})
	}

	render () {
		return (
			<div className="container updatemovie-form-container">
				<a href="/"><button className="btn btn-primary back-button">Back</button></a>
				<MovieForm 
					title={this.state.title}
					year={this.state.year}
					genre={this.state.genre}
					actors={this.state.actors}
					rating={this.state.rating}
					handleSubmit={this.handleSubmit}
					handleChange={this.handleChange}
					invalidYearFormat={this.state.invalidYearFormat}
					invalidRatingFormat={this.state.invalidRatingFormat}
					onDrop={this.onDrop}
					requiredFields={this.requiredFields}>
				</MovieForm>
				<HttpStatus
					status={this.state.status}
					statusMessage={this.state.statusMessage}>
				</HttpStatus>
			</div>
		)
	}
}