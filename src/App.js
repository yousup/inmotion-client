import React, { Component } from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap';
import './index.css';
import { AllMovies } from './all-movies';
import { NewMovie } from './new-movie';
import { UpdateMovie } from './update-movie';
import { MovieDetails } from './movie-details';
import { BrowserRouter as Router, Route, Link, Redirect } from "react-router-dom";
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="container text-xs-center">
        <Router>
						<div className="row titleBar">
							<div className="col">
								<h1><a href="/">Your amazing movie collection</a></h1>
							</div>
						</div>
          <Route exact path="/" component={AllMovies}/>
          <Route path="/new" component={NewMovie}/>
          <Route path="/update/:id" component={UpdateMovie}/>
          <Route path="/details/:id" component={MovieDetails}/>
        </Router>
      </div>
    );
  }
}

export default App;
